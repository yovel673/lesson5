#include "Menu.h"

int main()
{
	bool loop = true;
	int mainMenuChoice = 0;
	int shapeMenuChoice = 0;
	int modifyShapeChoice = 0;
	int modifyMenuChoice = 0;

	Menu m = Menu();

	while (loop)
	{
		system("cls");
		m.MainMenu();

		std::cin >> mainMenuChoice;

		switch (mainMenuChoice)
		{
		case 0:
			system("cls");
			m.ShapeMenu();

			std::cin >> shapeMenuChoice;
			switch (shapeMenuChoice)
			{
			case 0:
				m.addCircle();
				break;
			case 1:
				m.addArrow();
				break;
			case 2:
				m.addTriangle();
				break;
			case 3:
				m.addRectangle();
				break;
			}
			break;
		case 1:
			if (m.shapeAmount() != 0)
			{
				m.modifyShape();
			}
			break;

		case 2:
			if (m.shapeAmount() != 0)
			{
				m.deleteAllShapes();
			}
			break;

		case 3:
			exit(0);
			break;
		default:
			break;
		}
	}



	system("pause");


	return 0;
}