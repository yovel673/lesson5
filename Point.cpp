#include "Point.h"
#include <math.h>


/*
Class c'tor
*/
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}
/*new class c'tor because of erros*/
Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

/*
Class c'tor
*/
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

/*
Class d'tor
*/
Point::~Point()
{
	//blank. nothing to clear
}


/*
The opertor returns new point based on the sum of 2 points
*/
Point Point::operator+(const Point& other) const
{
	double xSum = this->_x + other.getX();
	double ySum = this->_y + other.getY();

	Point sumPoint = Point(xSum, ySum);
	return sumPoint;
	
}

/*
the opetator modifys the given point and addes other points to the current
*/
Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	return *this;
}
/*
class getter
*/
double Point::getX() const
{
	return _x;
}

/*
class getter
*/
double Point::getY() const
{
	return _y;
}
/*
the function return the distance between two points
*/
double Point::distance(const Point& other) const
{
	double xSum = pow(this->_x - other._x, 2);
	double ySum = pow(this->_y - other._y, 2);

	return sqrt(xSum + ySum);
}