#include "Circle.h"
#include "Point.h"

/*class c'tor*/
Circle::Circle(const Point& center, double radius, const string& type, const string& name):Shape(name, type)
{
	this->_radius = radius;
}
/*
class d'tor
*/
Circle::~Circle()
{
	//blank
}
/*
the function returns the point of the center of the circle
*/
const Point& Circle::getCenter() const
{
	return this->_center;
}
/*
The function return the radius of the circle
*/
double Circle::getRadius() const
{
	return this->_radius;
}
/*
The function is moving the circle
*/
void Circle::move(const Point& other)
{
	this->_center = other;
}
/*
The function is returining the area of the circle
*/
double Circle::getArea() const
{
	return (PI * pow(this->_radius, 2));
}
/*
The function is returning the permimeter of the circle
*/
double Circle::getPerimeter() const
{
	return (PI * 2 * this->getRadius());
}



void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
}


