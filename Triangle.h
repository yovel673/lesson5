#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	
	virtual void move(const Point& other);
	virtual double getArea() const;
	virtual double getPerimeter() const;

	void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	


private:
	Point _A;
	Point _B;
	Point _C;
	
};