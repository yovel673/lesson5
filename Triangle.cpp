#include "Triangle.h"



/*
Trinagle's c'tor
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name):Polygon(name, type)
{
	this->_A = a;
	this->_B = b;
	this->_C = c;

	_points.push_back(a);
	_points.push_back(a);
	_points.push_back(a);
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)  //given
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) //given
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}


Triangle::~Triangle()
{
	//blank
}
/*
The function moves the triangle
*/
void Triangle::move(const Point& other)
{
	this->_A += other;
	this->_B += other;
	this->_C += other;
}
/*
The function return the triangle area
*/
double Triangle::getArea() const
{
	double area =  abs((_A.getX() - _C.getX()) * (_B.getY() - _A.getY()) - (_A.getX() - _B.getX()) * (_C.getY() - _A.getY())) * 0.5;
	return area;
}
/*
the function return the permiter of the triangle
*/
double Triangle::getPerimeter() const
{
	double perimter = this->_A.distance(this->_B) + this->_A.distance(this->_C) + this->_B.distance(this->_C);
	return perimter;
}


