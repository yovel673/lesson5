#include "Rectangle.h"




void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) //given function
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) //given function
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


/*
Class c'tor
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name):Polygon(name, type)
{
	_points.push_back(a);
	_points.push_back(Point(a.getX() + length, a.getY() + width));
	this->_a = a;
	this->_length = length;
	this->_width = width;
}
/*
Class d'tor
*/
myShapes::Rectangle::~Rectangle()
{
	//blank
}


/*
The function moves the Rectangle
*/
void myShapes::Rectangle::move(const Point& other)
{
	this->_a += other;
}
/*
The function return the perimeter of the reactngle
*/

double myShapes::Rectangle::getPerimeter() const
{
	return (2 * this->_width) + (2 * _length);
}

/*
The function returns the area of the reactngle
*/
double myShapes::Rectangle::getArea() const
{
	return this->_width * this->_length;
}
