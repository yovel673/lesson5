#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

/*The function is addeing a circle*/
void Menu::addCircle()
{
	double x = 0;
	double y = 0;
	double radius = 0;
	string name;

	std::cout << "Enter x:" << endl;
	std::cin >> x;
	std::cout << "Enter y:" << endl;
	std::cin >> y;
	std::cout << "Enter radius:" << endl;
	std::cin >> radius;
	
	std::cout << "Enter the name of the shape:" << endl;
	std::cin >> name;


	
	this->_shapes.push_back(new Circle(Point(x, y), radius, "Circle", name));
	this->_shapes[_shapes.size() - 1]->draw(*_disp, *_board);

}
/*The function prints Arrow*/
void Menu::addArrow()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	string name;

	std::cout << "Enter x1:" << endl;
	std::cin >> x1;
	std::cout << "Enter y1:" << endl;
	std::cin >> y1;
	std::cout << "Enter x2:" << endl;
	std::cin >> x2;
	std::cout << "Enter y2:" << endl;
	std::cin >> y2;
	

	std::cout << "Enter the name of the shape:" << endl;
	std::cin >> name;


	this->_shapes.push_back(new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", name));
	this->_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
}
void Menu::addTriangle()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	double x3 = 0;
	double y3 = 0;
	string name;

	std::cout << "Enter x1:"<<endl;
	std::cin >> x1;
	std::cout << "Enter y1" << endl;
	std::cin >> y1;
	std::cout << "Enter x2:" << endl;
	std::cin >> x2;
	std::cout << "Enter y2:" << endl;
	std::cin >> y2;
	std::cout << "Enter x3:" << endl;
	std::cin >> x3;
	std::cout << "Enter y3:" << endl;
	std::cin >> y3;


	if ((y1 == y2 && y2 == y3) || (x1 == x2 && x2 == x3))
	{
		cout << "The points entered create a line." << endl;
		system("pause");

	}
	else
	{
		std::cout << "Enter the name of the shape:" << endl;
		std::cin >> name;
		this->_shapes.push_back(new Triangle(Point(x1, y1), Point(x2, y2),Point(x3, y3), "Triangle", name));
		this->_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
	}
}
void Menu::addRectangle()
{
	double x = 0;
	double y = 0;
	double length = 0;
	double width = 0;
	string name;
	cout << "Enter x:" << endl;
	cin >> x;
	cout << "Enter y:" << endl;
	cin >> y;
	cout << "Enter length:" << endl;
	cin >> length;
	cout << "Enter width:" << endl;
	cin >> width;

	cout << "Enter the name of the shape:" << endl;
	std::getline(std::cin, name);
	if (width == 0 || length == 0)
	{
		cout << "Length or Width can't be 0" << endl;
		system("pause");
	}
	else
	{
		this->_shapes.push_back(new myShapes::Rectangle(Point(x, y), length, width, "Rectangle", name));//add the shape to the vector
		this->_shapes[_shapes.size() - 1]->draw(*_disp, *_board);
	}
}

void Menu::modifyShape()
{

	int i = 0;
	int shapeChoice = 0;
	int menuChoice = 0;

	//for move option
	int x = 0;
	int y = 0;

	do
	{//making sure shapeChoice is valid
		system("cls");
		for (i = 0; i < _shapes.size(); i++)
		{ //printing details of each shape
			std::cout << "Enter " << i << " for " << _shapes[i]->getName() << " (" << _shapes[i]->getType() << ")\n";
		}

		std::cin >> shapeChoice;

	} while (shapeChoice < 0 || shapeChoice >= _shapes.size());

	std::cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape.\n";
	std::cin >> menuChoice;

	switch (menuChoice)
	{
	case 0:
		system("cls");
		std::cout << "Please enter the X moving scale: ";
		std::cin >> x;
		std::cout << "Please enter the Y moving scale: ";
		std::cin >> y;
		_shapes[shapeChoice]->clearDraw(*_disp, *_board);
		_shapes[shapeChoice]->move(Point(x, y));
		drawAll(); //redrawing all of the shapes including the moved one
		break;
	case 1:
		_shapes[shapeChoice]->printDetails();
		system("pause"); //pausing the screen so the user will se the details (there's a 'cls' after this)
		break;

	case 2:
		_shapes[shapeChoice]->clearDraw(*_disp, *_board);
		_shapes.erase(_shapes.begin() + shapeChoice); //I have to actually remove the shape. if not, the shape will get re-draw at drawAll()
		drawAll(); //we have to draw everything again, because when clearing the drawing, the drawing is just turning black and not actually get deleted
		break;
	}

}
void Menu::drawAll()
{
	int i = 0;

	for (i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}

}

void Menu::deleteAllShapes()
{
	int i = 0;

	for (i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->clearDraw(*_disp, *_board);
		_shapes.clear();
	}

}
int Menu::shapeAmount()
{
	return this->_shapes.size();
}
void Menu::MainMenu()
{
	std::cout << "Enter 0 to add a new shape." << endl << "Enter 1 to modify or get information from a current shape." << endl << "Enter 2 to delete all of the shapes." <<endl<< "Enter 3 to exit" << endl;
}
void Menu::ShapeMenu()
{
	std::cout << "Enter 0 to add a circle." << endl << "Enter 1 to add an arrow" << endl << "Enter 2 to add a triangle" << endl << "Enter 3 to add a rectangle" << endl;
}
