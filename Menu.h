#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	void addCircle();
	void addArrow();
	void addTriangle();
	void addRectangle();

	void modifyShape();//modifying a shape
	void drawAll(); //draws all of the shapes in _shapes
	void deleteAllShapes();
	int shapeAmount(); //returns _shapes.size()
	void MainMenu(); //printing the main menu text
	void ShapeMenu(); //printing the shape menu text


	

private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	std::vector<Shape *> _shapes;
};

