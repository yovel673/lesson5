#include "Arrow.h"



void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}
/*
Class c'tor
*/
Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name) :Shape(name, type)
{
	this->_p1 = a;
	this->_p2 = b;
}
/*
Class d'tor
*/
Arrow::~Arrow()
{
	//blank
}
/*
The function moves the arrow
*/
void Arrow::move(const Point& other)
{
	this->_p1 += other;
	this->_p2 += other;
}
/*
The function returns the area of the arrow
*/
double Arrow::getArea() const
{
	return 0; //because of the instructions
}
/*
The function return the perimeter of the arrow
*/
double Arrow::getPerimeter() const
{
	return this->_p1.distance(this->_p2); //because of the instructions
}

