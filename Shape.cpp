#include "Shape.h"
#include <string.h>
#include <iostream>
#include <string>
using namespace std;

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}

void Shape::printDetails() const
{
	cout << this->_type << "\t" << this->_name << "\t" << this->getArea() << "\t" << this->getPerimeter() << "\n";
}

